<?php
class Mdl_admin extends CI_Model{
    public function insertUser($data){
        $this->db->insert('tbl_users',$data);
        return $this->db->insert_id();
    }
    public function check_where($data)
    {   

        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('email_id',$data);
        //$this->db->where('user_type',1);
        $check_email=$this->db->get()->result_array();
        return $check_email;
    }
    public function loginvalidate($data)
    {
            $this->db->where($data);
            $details=$this->db->get('tbl_users')->row_array();

            if(!empty($details)){
                $usersession=array(
                    'name' => $details['name'],
                    'email_id' => $details['email_id'],
                    'user_id' => $details['user_id'],
                    'user_type' => $details['user_type']
                    );
                $this->session->set_userdata($usersession);
                return $details;
            }
            else{
                return false;
            }
                       
    }
    public function check_session(){

        if(!$this->session->userdata('email_id')){
            $this->session->sess_destroy();
            redirect('/');
        }

    }
    public function check_permission(){

        if($this->session->userdata('user_type') > 0){
            $this->session->sess_destroy();
            redirect('/');
        }

    }
    function sendMailToUser($toEmail, $subject, $mail_body, $fromEmail = '', $fromName = '', $ccEmails = '', $replyToEmail = '')
	{
		//require_once(APPPATH . 'libraries/smtp/class.phpmailer.php');
        require_once(APPPATH.'third_party/phpmailer/src/PHPMailer.php');
        require_once(APPPATH.'third_party/phpmailer/src/SMTP.php');

		if(!$fromEmail)
		$fromEmail = FROM_EMAIL;

		if(!$fromName)
		$fromName = FROM_EMAIL_NAME;

		$mail = new PHPMailer\PHPMailer\PHPMailer();
		$mail->IsSMTP();
		$mail->IsHTML(true); // send as HTML
		$mail->SMTPAuth = true;                  // enable SMTP authentication
		$mail->SMTPSecure = SMTP_SECURE;           // sets the prefix to the servier
		$mail->Host = SMTP_HOST;          // sets GMAIL as the SMTP server
		$mail->Port = SMTP_PORT;             // set the SMTP port
		$mail->Username = FROM_EMAIL;         // GMAIL username
		$mail->Password = SMTP_PASSWORD;         // GMAIL password
		$mail->From = $fromEmail;
		$mail->FromName = $fromName;
		if($replyToEmail != '')
		$mail->addReplyTo($replyToEmail, '');
		$mail->Subject = $subject;
		$mail->Body = $mail_body;            //HTML Body
		$emailId = $toEmail;
		 $emails  = explode(",", $emailId);
	    foreach($emails as $email){
		    $mail->AddAddress($toEmail);
		}
        if($mail->Send()){
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>