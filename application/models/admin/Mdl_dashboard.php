<?php
class Mdl_dashboard extends CI_Model{
    public function getActiveUsers(){
        $this->db->select("COUNT(user_id) AS active_users");
        $this->db->from('tbl_users');
        $this->db->where('is_verify',1);
        $this->db->where('is_active',1);
        $this->db->where('user_type',1);
        $this->db->where('is_del',0);
        return $this->db->get()->row()->active_users;
        //echo $this->db->last_query();die;
    }
    public function getActiveProductWithUsers(){
        $this->db->select("COUNT(PI.p_id) AS active_productswithusers");
        $this->db->from('tbl_pick_product AS PI');
        $this->db->join('tbl_users AS U','U.user_id=PI.user_id','left');
        $this->db->join('tbl_products AS P','P.product_id=PI.product_id','left');
        $this->db->where('U.is_verify',1);
        $this->db->where('U.is_active',1);
        $this->db->where('U.user_type',1);
        $this->db->where('U.is_del',0);
        $this->db->where('P.status',1);
        return $this->db->get()->row()->active_productswithusers;
        //echo $this->db->last_query();die;
    }
    public function getActiveProducts(){
        $this->db->select("COUNT(product_id) AS active_products");
        $this->db->from('tbl_products');
        $this->db->where('status',1);
        return $this->db->get()->row()->active_products;
        //echo $this->db->last_query();die;
    }
    public function getActiveProductWithNoUsers(){
        $this->db->select("COUNT(P.product_id) AS active_productswithnousers");
        $this->db->from('tbl_products AS P');
        $this->db->where('P.product_id NOT IN (SELECT PI.product_id from tbl_pick_product AS PI)');
        $this->db->where('P.status',1);
        return $this->db->get()->row()->active_productswithnousers;
        //echo $this->db->last_query();die;
    }
    public function getamountActiveProducts(){
        $this->db->select("SUM(PI.qty) AS active_amountproducts");
        $this->db->from('tbl_pick_product AS PI');
        $this->db->join('tbl_products AS P','P.product_id=PI.product_id','left');
        $this->db->join('tbl_users AS U','U.user_id=PI.user_id','left'); 
        $this->db->where('U.is_active',1);
        $this->db->where('U.is_del',0);        
        $this->db->where('P.status',1);
        $this->db->where('U.is_active',1);
        $this->db->where('U.is_del',0);
        return $this->db->get()->row()->active_amountproducts;
        //echo $this->db->last_query();die;
    }
    public function getPriceAmountActiveProducts(){
        $this->db->select("SUM(PI.qty * PI.price) AS active_priceamountproducts");
        $this->db->from('tbl_pick_product AS PI');
        $this->db->join('tbl_products AS P','P.product_id=PI.product_id','left');
        $this->db->join('tbl_users AS U','U.user_id=PI.user_id','left');        
        $this->db->where('P.status',1);
        $this->db->where('U.is_active',1);
        $this->db->where('U.is_del',0);
        return $this->db->get()->row()->active_priceamountproducts;
        //echo $this->db->last_query();die;
    }
    public function activePriceAmountProductsPerUser(){
        $this->db->select("U.name,SUM(PI.qty * PI.price) AS total_price");
        $this->db->from('tbl_pick_product AS PI');
        $this->db->join('tbl_products AS P','P.product_id=PI.product_id','left');
        $this->db->join('tbl_users AS U','U.user_id=PI.user_id','left');        
        $this->db->where('P.status',1);
        $this->db->where('U.is_active',1);
        $this->db->where('U.is_del',0);
        $this->db->group_by('U.user_id');
        return $this->db->get()->result_array();
        //echo $this->db->last_query();die;
    }

}
?>