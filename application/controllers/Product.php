<?php
class Product extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/mdl_admin');
        $this->load->model('admin/mdl_product');
        $this->mdl_admin->check_session();
        //$this->session->sess_destroy();    

    }
    public function index()
    {
       //$this->load->view('Dashboard');
    }
    public function pickProductFromList(){
        $data['menu'] = 'pick-product';
        $productList = $this->db->get_where('tbl_products',array('status'=> 1 ))->result_array();
        $data['products'] = $productList;
        $this->load->view('pick_product',$data);
    }
    public function addProductFromList(){
        if($_POST){
            $exchangeCurr = $this->exchangeCurrency();
            $AddNew = array(
                'product_id' => $this->input->post('productList'),
                'user_id' => $this->session->userdata('user_id'),
                //'price' => $this->input->post('productPrice'),
                'price' => round(($exchangeCurr) * $this->input->post('productPrice'),2),
                'qty' => $this->input->post('productQty'),
                'date_added' => date('Y-m-d H:i:s')
            );
            $productId = $this->mdl_product->InsertProductFromList($AddNew);
            if($productId){
                $this->session->set_flashdata('success','Product added sucessfully.');
			    redirect('product/pick');
            }
        }else{
            $this->session->set_flashdata('success','Something went wrong!...');
            redirect('product/pick');
        }
    }
    public function exchangeCurrency(){ 
        $endpoint = 'latest';
        $access_key = API_KEY;
        $symbols = 'USD';
        // initialize CURL:
        $ch = curl_init('http://api.exchangeratesapi.io/v1/'.$endpoint.'?access_key='.$access_key.'&symbols='.$symbols.'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $jsonResponse = curl_exec($ch);
        curl_close($ch);
        $exchangeResponse = json_decode($jsonResponse, true);
        return ($exchangeResponse['rates']['USD']);
    }
}
?>