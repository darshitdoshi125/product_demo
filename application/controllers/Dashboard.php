<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/mdl_admin');
        $this->load->model('admin/mdl_dashboard');
        $this->mdl_admin->check_session();
        $this->mdl_admin->check_permission();    

    }

    public function index()
    {
        $data['menu'] = 'dashboard';
        $data['activeUsers'] = $this->mdl_dashboard->getActiveUsers();
        $data['activeUsersAttachProduct'] = $this->mdl_dashboard->getActiveProductWithUsers();
        $data['activeProducts'] = $this->mdl_dashboard->getActiveProducts();
        $data['activeProductsNouser'] = $this->mdl_dashboard->getActiveProductWithNoUsers();
        $data['activeAmountProducts'] = $this->mdl_dashboard->getamountActiveProducts();
        $data['activePriceAmountProducts'] = $this->mdl_dashboard->getPriceAmountActiveProducts();
        $data['activePriceAmountProductsPerUser'] = $this->mdl_dashboard->activePriceAmountProductsPerUser();
       $this->load->view('Dashboard',$data);
    }

}
