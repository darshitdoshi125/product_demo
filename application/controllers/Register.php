<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	
	public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/mdl_admin');
		// if($this->session->userdata('email_id')){
		// 	redirect('/dashboard');
		// }
    }
    public function index(){
        $this->load->view('admin/register',true);
    }
	public function adduser()
	{
		$email=$this->input->post('email');
			$newUser=array(
			'name' =>$this->input->post('username'),
			'email_id' =>$this->input->post('email'),
			'password' =>md5($this->input->post('password')), // 123456	
			'user_type' => 1	
			);
			if($this->mdl_admin->insertUser($newUser))
			{
				$mail_data['email_id'] = $newUser['email_id'];
				$mail_data['link'] = base_url(). "verify/" . md5($newUser['email_id']);
				$view = $this->load->view('emailtemplate',$mail_data ,true);	
				//$send_mail = $this->mdl_admin->sendMailToUser('darshitdoshi125@gmail.com','Verify user','Please verify your account  '.$mail_data['link'],'thelyrics1993@gmail.com','product test task');
				$send_mail = $this->mdl_admin->sendMailToUser($newUser['email_id'],'Verify user',$view,FROM_EMAIL,FROM_EMAIL_NAME);
				//print_r($send_mail);die;
				$this->session->set_flashdata('success','User registered successfully');
				redirect('/');
			}
	}
	public function check_email()
	{
		$email=$this->input->post('email');
		$check=$this->mdl_admin->check_where($email);
		if(empty($check)){
			echo json_encode(true);	
		}else{
			echo json_encode("email address allready exists");	
		}
	}

	public function login_check()
	{		
		$data=array(	
			'email_id' =>$this->input->post('email'),
			'password' =>md5($this->input->post('password')),// 123456
			'is_verify' => 1
			);

		$checkRecord=$this->mdl_admin->loginvalidate($data);
		if(!empty($checkRecord)){
			if($checkRecord['user_type'] == '0'){
				redirect('/dashboard');
			}else{
				redirect('/product/pick');
			}
		}
		else
		{
			$this->session->set_flashdata('error','Invalid Creadentials');
			redirect('/');
		}

	}
	public function verifyUserViaMail($eId){
		$ifEmailRegistered = $this->db->get_where('tbl_users',array('user_type'=> 1,'md5(email_id)'=> $eId, 'is_verify'=> 0 ))->row_array();
		if(!empty($ifEmailRegistered)){
			$this->db->where('md5(email_id)', $eId);
			$this->db->where('user_id', $ifEmailRegistered['user_id']);
			$this->db->where('is_verify', 0);
			$this->db->where('user_type', 1);
			$this->db->update('tbl_users', array('is_verify'=>1));
			echo "Your email address successfully verified... Thank you for register with us.";
		}else{
			echo "Something went wrong, Please contact to admin";
		}
	}

}
