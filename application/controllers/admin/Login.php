<?php
defined('BASEPATH') OR exit('invalid request');
class Login extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/mdl_admin');
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }
}
?>