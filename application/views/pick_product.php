<?php $this->load->view('header') ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"> Pick product from list</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add new to your list</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="pick-product-form" name="pick-product-form" action="<?php echo base_url() ?>addProductFromList" method="post">
                
                <div class="card-body">
                <div class="form-group">
                    <label for="">Select product</label>
                    <select class="form-control" name="productList" id="productList">
                      <option value="">Select product</option>
                      <?php
                      if(!empty($products)){
                        foreach($products as $product){?>
                            <option value="<?php echo $product['product_id'];?>"><?php echo $product['title'];?></option>
                        <?php
                        }
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Quantity</label>
                    <input type="number" class="form-control" name="productQty" id="productQty" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="">price</label>
                    <input type="text" class="form-control" name="productPrice" id="productPrice" placeholder="Please enter product price">
                  </div>
                  
                <!-- /.card-body -->

                <div class="card-footer">
                  <input type="submit" name="addProduct" id="addProduct" class="btn btn-primary" value="Add Product">
                </div>
              </form>
            </div>
            
            
          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script>

  $( "#pick-product-form").validate({
  rules: {
    productList: {
      required: true,      
    },
    productQty:{
      required: true,
    },
    productPrice:{
      required: true,
      number: true
    }
  }
});
</script>
<?php if ($this->session->flashdata('success')): ?>
<script>
swal({
  title: "Success",
  text: '<?php echo $this->session->flashdata('success') ?>',
  button: true,
  timer: 5000,
});
</script>
<?php endif; ?>
  <!-- /.content-wrapper -->
  <?php $this->load->view('footer') ?>  